<html>

<head>
    <title>Date Time Function</title>
</head>

<body>

    <form action="DateTimeFunction.php" method="POST">
        <p>
            First user: <input type="text" name="name_01"><br><br>
            First user's Birthday <input type="date" name="birthday_01"><br><br>

            Second user: <input type="text" name="name_02"><br><br>
            Second user's Birthday <input type="date" name="birthday_02"><br><br>

            <input type="submit" value="Send">&nbsp
            <input type="reset" value="Reset">
        </p>
    </form>

    <?php

    $name_01 = "";
    $name_02 = "";
    $birthday_01 = "";
    $birthday_02 = "";

    if (isset($_POST["name_01"])) {
        $name_01 = $_POST["name_01"];
    }

    if (isset($_POST["name_02"])) {
        $name_02 = $_POST["name_02"];
    }

    if (isset($_POST["birthday_01"])) {
        $birthday_01 = $_POST["birthday_01"];
    }

    if (isset($_POST["birthday_02"])) {
        $birthday_02 = $_POST["birthday_02"];
    }

    print "First user's name: $name_01<br>";
    print "First user's birthday: $birthday_01<br>";
    print "Second user's name: $name_02<br>";
    print "Second user's birthday: $birthday_02<br>";

    function Birthday($birthday, $birthday1, $birthday2){

        $date = date_format(date_create($birthday), "l, F d, Y");
        print "Birthday: $date <br>";

        $diff= date_diff(date_create($birthday1), date_create($birthday2))->format("%a days");
        print "Date diff: $diff <br>";

        $age = date_diff(date_create($birthday), date_create("now"))->y;
        print "User age: $age <br>";

        $agebetween = date_diff(date_create($birthday1), date_create($birthday2))->y;
        print "Age between: $agebetween <br>";
    }

    print "<br>More Information: <br>";
    print "User 1<br>";
    Birthday($birthday_01,$birthday_01, $birthday_02);

    print "<br>User 2<br>";
    Birthday($birthday_02,$birthday_01, $birthday_02);
    ?>

</body>

</html>