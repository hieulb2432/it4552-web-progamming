<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Convert</title>
</head>

<body>
    <?php

    $radian = 0;
    $degree = 0;
    if (isset($_POST["radian"])) {
        $radian = $_POST["radian"];
    }
    if (isset($_POST["degree"])) {
        $degree = $_POST["degree"];
    }

    function Convert_01($radian)
    {
        $pi = 3.14159;
        return ($radian * (180 / $pi));
    }

    function Convert_02($degree)
    {
        $pi = 3.14159;
        return ($degree * ($pi / 180));
    }
    ?>

    <form action="ex_05.php" method="POST">
        <p>
            Enter radian: <input type="number" name="radian"><br>
            <?php
            $result_01 = Convert_01($radian);
            print "Result: $result_01";
            ?>

            <br><br>Enter degree: <input type="number" name="degree"><br>
            <?php
            $result_02 = Convert_02($degree);
            print "Result: $result_02";
            ?>

            <br><br><input type="submit" value="Send">&nbsp
            <input type="reset" value="Reset">

        </p>
    </form>

</body>

</html>