<html>

<head>
    <title>Date Time Validation</title>
</head>

<body>
    <?php
    $name = "";
    $day = 0;
    $month = 0;
    $year = 0;
    $hour = 0;
    $minutes = 0;
    $seconds = 0;

    if (isset($_POST["name"])) {
        $name = $_POST["name"];
    }
    if (isset($_POST["day"])) {
        $day = $_POST["day"];
    }
    if (isset($_POST["month"])) {
        $month = $_POST["month"];
    }
    if (isset($_POST["year"])) {
        $year = $_POST["year"];
    }

    if (isset($_POST["hour"])) {
        $hour = $_POST["hour"];
    }
    if (isset($_POST["minutes"])) {
        $minutes = $_POST["minutes"];
    }
    if (isset($_POST["seconds"])) {
        $seconds = $_POST["seconds"];
    }

    ?>

    <form action="ex_04.php" method="POST">
        <p>
            Your name: <input type="text" name="name"><br><br>

            Date: <input type="number" name="day" min="1" max="31" value="'.$day.'">
            <input type="number" name="month" min="1" max="12" value="'.$month.'">
            <input type="number" name="year" min="1" max="3000" value="'.$year.'"><br><br>

            Time: <input type="number" name="hour" min="1" max="24" value="'.$hour.'">
            <input type="number" name="minutes" min="1" max="60" value="'.$minutes.'">
            <input type="number" name="seconds" min="1" max="60" value="'.$seconds.'"><br><br>

            <input type="submit" value="Send">&nbsp
            <input type="reset" value="Reset">



        </p>
    </form>

    <?php
    print "Hi $name! <br>";
    print "You have choose to have an appoinment on $hour:$minutes:$seconds, $day/$month/$year <br><br>";
    print "More information<br>";
    if($hour>12){
        $hour1 = $hour - 12;
        print "In 12 hour, the time and date is $hour1:$minutes:$seconds PM, $day/$month/$year <br>";
    } else print "In 12 hours, the time and date is $hour:$minutes:$seconds AM, $day/$month/$year <br>";

    switch($month){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            print "This month has 31 days<br>";
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            print "This month has 30 days<br>";
            break;
        case 2: 
            $check = ($year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0);
            if($check == true){
            print "This month has 29 days<br>";
            } else print "This month has 28 days<br>";
    }
    ?>
</body>

</html>