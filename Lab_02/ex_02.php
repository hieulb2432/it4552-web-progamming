<?php

$string_input = "";
$string_output = "";
$a = "*";

if (isset($_GET["string_input"])) {
    $string_input = $_GET["string_input"];
}
if (isset($_GET["string_output"])) {
    $string_output = $_GET["string_output"];
}
if (isset($_GET["a"])) {
    $a = $_GET["a"];
}


switch ($a) {
    case "1":
        $string_output = strlen($string_input);
        break;
    case "2":
        $string_output = trim($string_input);
        break;
    case "3":
        $string_output = strtoupper($string_input);
        break;
    case "4":
        $string_output = strtolower($string_input);
        break;
}

echo '<form action = "ex_02.php" method="GET">
<p>
    <input type="text" name="string_input" value="'.$string_input.'"><br><br>

    
    <input type="radio" id="1" name="a" value="1">
    <label for="1">strlen</label>
    <input type="radio" id="2" name="a" value="2">
    <label for="2">trim</label>
    <input type="radio" id="3" name="a" value="3">
    <label for="3">strtoupper</label>
    <input type="radio" id="4" name="a" value="4">
    <label for="4">strtolower</label> <br><br>

    <input type="submit" value="Send">
    <h2> Result : ' . $string_output . '</h2>
</p>
</form>'

?>