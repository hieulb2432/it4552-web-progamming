<?php
$submitted = false;
$sort_type = "sort";

if (isset($_POST['submitted'])) {
    $submitted = true;
    if (isset($_POST['sort_type']))
        $sort_type = $_POST['sort_type'];
}

function user_sort($a, $b)
{
    // smarts is all-important, so sort it first
    if ($b == 'smarts') {
        return 1;
    } else if ($a == 'smarts') {
        return -1;
    }
    return ($a == $b) ? 0 : (($a < $b) ? -1 : 1);
}
$values = array(
    'name' => 'Buzz Lightyear',
    'email_address' => 'buzz@starcommand.gal',
    'age' => 32,
    'smarts' => 'some'
);
// if ($submitted) {
//     if ($sort_type == 'usort' || $sort_type == 'uksort' || $sort_type == 'uasort') {
//         $sort_type($values, 'user_sort');
//     } else {
//         $sort_type($values);
//     } 
// }

if ($submitted) {
    switch ($sort_type) {
        case "sort":
            sort($values, 1);
            break;
        case "rsort":
            rsort($values, 1);
            break;
            // case "usort":

            //     break;
        case "ksort":
            ksort($values, 1);
            break;
        case "krsort":
            krsort($values, 1);
            break;
            // case "uksort":

            //     break;
        case "asort":
            asort($values, 1);
            break;
        case "arsort":
            arsort($values, 1);
            break;
            // case "uasort":

            //     break;
    }
}

?>

<form action="ex_01.php" method="post">
    <p>

        <input type="radio" name="sort_type" value="sort" <?= $sort_type == 'sort' ? "checked='checked'" : '' ?> />Standard sort<br />
        <input type="radio" name="sort_type" value="rsort" <?= $sort_type == 'rsort' ? "checked='checked'" : '' ?> /> Reverse sort<br />
        <input type="radio" name="sort_type" value="usort" <?= $sort_type == 'usort' ? "checked='checked'" : '' ?> />User-defined sort<br />
        <input type="radio" name="sort_type" value="ksort" <?= $sort_type == 'ksort' ? "checked='checked'" : '' ?> /> Key sort<br />
        <input type="radio" name="sort_type" value="krsort" <?= $sort_type == 'krsort' ? "checked='checked'" : '' ?> />Reverse key sort<br />
        <input type="radio" name="sort_type" value="uksort" <?= $sort_type == 'uksort' ? "checked='checked'" : '' ?> />User-defined key sort<br />
        <input type="radio" name="sort_type" value="asort" <?= $sort_type == 'asort' ? "checked='checked'" : '' ?> /> Value sort<br />
        <input type="radio" name="sort_type" value="arsort" <?= $sort_type == 'arsort' ? "checked='checked'" : '' ?> />Reverse value sort<br />
        <input type="radio" name="sort_type" value="uasort" <?= $sort_type == 'uasort' ? "checked='checked'" : '' ?> />User-defined value sort<br />

    </p>
    <p align="left">
        <input type="submit" value="Sort" name="submitted" />
    </p>

    <p>
        Values <?= $submitted ? "sorted by $sort_type" : "unsorted"; ?>
    </p>

    <ul>
        <?php
        foreach ($values as $key => $value) {
            echo "<li><b>$key</b>: $value</li>";
        }
        ?>
    </ul>
</form>

