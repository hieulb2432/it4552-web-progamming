<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Message Board</title>
</head>

<style>
    .error {
        color: #FF0000;
    }

    .response {
        padding: 10px;
        margin-top: 10px;
        border-radius: 2px;
    }

    div {
        display: table;
        border: solid;
        width: 300px;
        height: 100px;
        background-color: #ffeedd;
    }

    img {
        vertical-align: middle;
        display: table-cell;
    }

    div span {
        vertical-align: middle;
        display: table-cell;
    }
</style>

<?php
function checkImage($fileImage)
{
    $validext = array('gif', 'jpg', 'jpeg', 'png');
    $ext = strtolower(pathinfo($fileImage, PATHINFO_EXTENSION));

    // Validate file input to check if is with valid extension
    if (!in_array($ext, $validext)) {
        $response = array(
            "type" => "error",
            "message" => "Upload valiid images. PNG, JPEG, JPG, GIF are allowed."
        );
    }
}
?>

<?php
$target_dir = "posts/";
$username = $textdata = "";
$textErr = $nameErr = "";

if (isset($_POST['submit'])) {
    $time = time();
    $filename = $target_dir . "post_" . $time . ".txt";

    $username = $_POST['username'];
    $textdata = $_POST['textdata'];
    $check = 1;
    if (empty($username)) {
        $nameErr = "Name is required";
        $check = 0;
    }
    if (!strlen(trim($_POST['textdata']))) {
        $textErr = "Content is required";
        $check = 0;
    }
    if ($check) {
        // Upload text
        $content = $username . " says:" . $textdata;
        $textFile = fopen($filename, "w") or die("Unable to open file!");
        fwrite($textFile, $content);
        fclose($textFile);

        if ($_FILES['fileUpload']['size'] > 0) {
            // Upload image
            $fileImage = $_FILES['fileUpload']['name'];
            $ext = end((explode(".", $fileImage)));
            $targetImage = $target_dir . "post_" . $time . "." . $ext;
            if (move_uploaded_file($_FILES["fileUpload"]["tmp_name"], $targetImage)) {
                $response = array(
                    "type" => "success",
                    "message" => "Image uploaded successfully."
                );
            } else {
                $response = array(
                    "type" => "error",
                    "message" => "Problem in uploading image files."
                );
            }
        }
    }
}

?>

<body>
    <h1>Message Board</h1>

    <!-- Display messages -->
    <?php
    if (count(glob("posts/*.txt")) <= 0) {
        echo "Chưa có thông điệp nào được đăng <br><br>";
    } else {
        foreach (glob("posts/*.txt") as $file) {
            $fileText = $file;
            $contents = file_get_contents($fileText);
            $imgFileName = end((explode("/", $fileText)));
            $imgFileName = str_replace(".txt", "", $imgFileName);
            $filePosts = glob("posts/$imgFileName.*");

            // Check have image file
            if (count($filePosts) == 1) {
                echo '<div><span>';
                echo $contents;
                echo '</span></div><br>';
            } else {
                foreach ($filePosts as $filePost) {
                    $info = pathinfo($filePost);
                    if ($info['extension'] != "txt") {
                        $fileImage = $filePost;
                        break;
                    }
                }
    ?>
                <div>
                    <span><img src="<?php echo $fileImage ?>" width="100" height="100"></span>
                    <span><?php echo $contents; ?></span>
                </div><br>
            <?php
            } ?>
        <?php } ?>
    <?php } ?>

    <form action="MessageBoard.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Đăng tải một thông điệp mới</legend> <br>
            Tên người dùng:<br>
            <input type="text" name="username" value="" size="40">
            <span class="error">* <?php echo $nameErr; ?></span><br>
            <input type="checkbox" name="noname" value=""> Đăng thông điệp vô danh
            <br><br>
            Nội dung thông điệp:<br>
            <textarea rows="9" cols="70" name="textdata"> </textarea>
            <span class="error">* <?php echo $textErr; ?></span>
            <br><br>
            Hình ảnh kèm theo (tuỳ chọn):
            <br>
            <input type="file" name="fileUpload">
            <br><br>
            <input type="submit" name="submit" value="Đăng thông điệp">
        </fieldset>
    </form>

    <?php if (!empty($response)) { ?>
        <p class="response <?php echo $response["type"]; ?>"><?php echo $response["message"]; ?></p>
    <?php } ?>
</body>

</html>